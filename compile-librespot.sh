#!/bin/bash

set -e

apt update
apt install -y build-essential libasound2-dev git libavahi-compat-libdnssd-dev pkg-config

git clone https://github.com/librespot-org/librespot.git

cd librespot

#git checkout 299b7dec20b45b9fa19a4a46252079e8a8b7a8ba
git checkout v0.6.0

#cargo build --release --no-default-features --features with-dns-sd 
#cargo build --release --no-default-features
cargo build --release --no-default-features --features 'alsa-backend with-dns-sd'
#cargo build --release --features with-dns-sd

apt-get clean && rm -fR /var/lib/apt/lists
